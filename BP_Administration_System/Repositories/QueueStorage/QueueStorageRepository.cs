﻿using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.QueueStorage
{
    //Repozitar zakladnich operaci s QueueStorage
    public class QueueStorageRepository : QueueStorageAdapter, IQueueStorageRepository
    {
        public QueueStorageRepository(IOptions<StorageSettings> QueueStorageConfig, ILogger<QueueStorageAdapter> logger) : base(QueueStorageConfig, logger) 
        {

        }

        //Pridani zpravy do fronty
        public async Task AddMessageAsync(string message)
        {
            await queue.AddMessageAsync(new CloudQueueMessage(message));
        }
    }
}
