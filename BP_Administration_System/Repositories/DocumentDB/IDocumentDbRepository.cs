﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Interface zakladnich CRUD operaci
    public interface IDocumentDbRepository<TModel, TId>
    {
        Task<IEnumerable<TModel>> GetItemsAsync();
        Task<TModel> GetItemAsync(TId id);
        Task<TModel> AddDocumentAsync(TModel item);
        Task<TModel> UpdateDocumentAsync(TId id, TModel item);
        Task DeleteDocumentAsync(TId id);
    }
}
