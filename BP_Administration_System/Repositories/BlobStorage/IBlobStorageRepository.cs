﻿using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.BlobStorage
{
    //Interface zakladnich operaci pro praci s bloby
    public interface IBlobStorageRepository
    {
        Task<IEnumerable<CloudBlockBlob>> GetAllBlobsAsync();
        CloudBlockBlob GetBlob(string blobName);
        Task<CloudBlockBlob> UploadBlobAsync(IFormFile file);
        Task<MemoryStream> DownloadBlobAsync(string blobName);
        Task DeleteBlobAsync(string blobName);
    }
}
