﻿namespace BP_Administration_System.Models
{
    //Model pro nastaveni DocumentDB
    public class DocumentDbSettings
    {
        public string DatabaseName { get; set; }
        public string CollectionName { get; set; }
        public string EndpointUri { get; set; }
        public string AuthKey { get; set; }
    }
}
