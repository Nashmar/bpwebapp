﻿using BP_Administration_System.Models;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Abstraktni trida pro komunikaci s DocumentDB
    public abstract class DocumentDbAdapter
    {
        protected readonly string AuthKey;
        protected readonly string EndpointUri;
        protected readonly string DatabaseId;
        protected readonly string CollectionId;
        protected static DocumentClient client;
        protected readonly ILogger _logger;


        public DocumentDbAdapter(IOptions<DocumentDbSettings> DocumentDbConfig, ILogger<DocumentDbAdapter> logger)
        {
            this.DatabaseId = DocumentDbConfig.Value.DatabaseName;
            this.CollectionId = DocumentDbConfig.Value.CollectionName;
            this.AuthKey = DocumentDbConfig.Value.AuthKey;
            this.EndpointUri = DocumentDbConfig.Value.EndpointUri;
            client = new DocumentClient(new Uri(EndpointUri), AuthKey);
            _logger = logger;
        }
    }
}
