﻿using BP_Administration_System.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Repozitar zakladnich CRUD operaci pro Employees
    public class EmployeeRepository : DocumentDbAdapter, IEmployeeDocumentDbRepository
    {
        public EmployeeRepository(IOptions<DocumentDbSettings> DocumentDbConfig, ILogger<DocumentDbAdapter> logger) : base(DocumentDbConfig, logger)
        {

        }

        //Precteni vsech dokumentu typu Employee
        public async Task<IEnumerable<Employee>> GetItemsAsync()
        {
            IDocumentQuery<Employee> query = client.CreateDocumentQuery<Employee>(
            UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId))
            .Where(emp => emp.DocType == nameof(Employee))
            .AsDocumentQuery();

            List<Employee> results = new List<Employee>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<Employee>());
            }
            return results;
        }

        //Precteni konkretniho dokumentu typu Employee na zaklade konkretniho id
        public async Task<Employee> GetItemAsync(string id)
        {
            try
            {
                Document document = await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
                return (Employee)(dynamic)document;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("Cannot GET employee with id {0} in {1}", id, nameof(GetItemAsync));
                    return null;
                }
                else
                {
                    _logger.LogError("Error in {0}, Error: {1}", nameof(GetItemAsync), e.ToString());
                    throw e;                   
                }
            }
        }

        //Pridani dokumentu Employee
        public async Task<Employee> AddDocumentAsync(Employee item)
        {
            try
            {
                var document = await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId), item);
                var employee = JsonConvert.DeserializeObject<Employee>(document.Resource.ToString());
                return employee;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(AddDocumentAsync), ex.ToString());
                throw ex;
            }
        }

        //Aktualizovani konkretniho dokumentu Employee na zaklade konkretniho id
        public async Task<Employee> UpdateDocumentAsync(string id, Employee item)
        {
            try
            {
                var document = await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id), item);
                var employee = JsonConvert.DeserializeObject<Employee>(document.Resource.ToString());
                return employee;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(UpdateDocumentAsync), ex.ToString());
                throw ex;
            }
        }

        //Smazani dokumentu typu Employee
        public async Task DeleteDocumentAsync(string id)
        {
            await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
        }

        //Ziskani aktualni kapacity Meeting Roomu do ktereho patri dany Employee
        public int GetActualCapacityOfRoom(IEnumerable<Employee> employees, MeetingRoom meetingRoom)
        {
            return employees.Where(emp => emp.MeetingRoom.MeetingRoomId == meetingRoom.MeetingRoomId).Count();
        }

        //Srovnani Employees podle specifickych kriterii
        public IEnumerable<Employee> SortBy<T>(IEnumerable<Employee> employees, Func<Employee, T> condition)
        {
            return employees.OrderBy(condition);
        }

    }
}
