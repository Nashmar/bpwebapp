﻿using BP_Administration_System.Models;
using BP_Administration_System.Models.TableStorage;
using BP_Administration_System.Repositories;
using BP_Administration_System.Repositories.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace BP_Administration_System.Data
{
    //Trida pro inicializaci a naseedovani Table Storage
    public class TableStorageInitializer : TableStorageAdapter
    {
        public TableStorageInitializer(IOptions<StorageSettings> TableStorageConfig, ILogger<TableStorageAdapter> logger) : base(TableStorageConfig, logger)
        {

        }

        //Inicializacni metoda
        public void Initialize(IEnumerable<Employee> employees)
        {
            CreateTableAsync().Wait();
            InsertAppSettingsEntites(employees.ToList()).Wait();
            InsertProfileSettingsEntities(employees.ToList()).Wait();
        }


        //Vytvori tabulku pokud neexistuje
        public async Task CreateTableAsync()
        {
            try
            {
                await table.CreateIfNotExistsAsync();
            }
            catch (StorageException ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(CreateTableAsync), ex.ToString());
                throw ex;
            }
        }

        //Metoda pro naseedovani Entit ProfileSettings
        private List<ProfileSettings> SeedProfileSettings(List<Employee> employees)
        {
            return new List<ProfileSettings>()
            {
            new ProfileSettings(employees[0].LastName, "ProfileSettings") { Nickname = "MannzT", Email = "Mann@domain.com", Newsletter = "No" },
            new ProfileSettings(employees[1].LastName, "ProfileSettings") { Nickname = "Arny12", Email = "Arny@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[2].LastName, "ProfileSettings") { Nickname = "Joneser", Email = "FJones@domain.com", Newsletter = "No" },
            new ProfileSettings(employees[3].LastName, "ProfileSettings") { Nickname = "Forder", Email = "Ford@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[4].LastName, "ProfileSettings") { Nickname = "Steve99", Email = "Stevens@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[5].LastName, "ProfileSettings") { Nickname = "Boyder", Email = "BoyderAustin@domain.com", Newsletter = "No" },
            new ProfileSettings(employees[6].LastName, "ProfileSettings") { Nickname = "Nose75", Email = "Nose@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[7].LastName, "ProfileSettings") { Nickname = "PGray", Email = "PeterGray@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[8].LastName, "ProfileSettings") { Nickname = "Dory", Email = "Dorian@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[9].LastName, "ProfileSettings") { Nickname = "Billy", Email = "Travolta@domain.com", Newsletter = "No" },
            new ProfileSettings(employees[10].LastName, "ProfileSettings") { Nickname = "JarredT", Email = "JarredToews@domain.com", Newsletter = "Yes" },
            new ProfileSettings(employees[11].LastName, "ProfileSettings") { Nickname = "Zaccy", Email = "ZacA@domain.com", Newsletter = "No" }
            };
        }

        //Metoda pro naseedovani Entit ApplicationSettings
        private List<ApplicationSettings> SeedApplicationSettings(List<Employee> employees)
        {
            return new List<ApplicationSettings>()
            {
            new ApplicationSettings(employees[0].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "EN" },
            new ApplicationSettings(employees[1].LastName, "ApplicationSettings") { DesignTheme = "Light", Language = "DE" },
            new ApplicationSettings(employees[2].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "CZ" },
            new ApplicationSettings(employees[3].LastName, "ApplicationSettings") { DesignTheme = "Light", Language = "EN" },
            new ApplicationSettings(employees[4].LastName, "ApplicationSettings") { DesignTheme = "Light", Language = "EN" },
            new ApplicationSettings(employees[5].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "DE" },
            new ApplicationSettings(employees[6].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "DE" },
            new ApplicationSettings(employees[7].LastName, "ApplicationSettings") { DesignTheme = "Light", Language = "EN" },
            new ApplicationSettings(employees[8].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "CZ" },
            new ApplicationSettings(employees[9].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "EN" },
            new ApplicationSettings(employees[10].LastName, "ApplicationSettings") { DesignTheme = "Light", Language = "EN" },
            new ApplicationSettings(employees[11].LastName, "ApplicationSettings") { DesignTheme = "Dark", Language = "CZ" }
            };
        }

        //Vlozeni entit ApplicationSettings do tabulky
        public async Task InsertAppSettingsEntites(List<Employee> employees)
        {
            for (int i = 0; i < SeedApplicationSettings(employees).Count; i++)
            {
                TableOperation tableOperation = TableOperation.InsertOrReplace(SeedApplicationSettings(employees)[i]);
                await table.ExecuteAsync(tableOperation);
            }
        }

        //Vlozeni entit ProfileSettings do tabulky
        public async Task InsertProfileSettingsEntities(List<Employee> employees)
        {
            for (int i = 0; i < SeedProfileSettings(employees).Count; i++)
            {
                TableOperation tableOperation = TableOperation.InsertOrReplace(SeedProfileSettings(employees)[i]);
                await table.ExecuteAsync(tableOperation);
            }
        }
    }
}
