﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageResizer;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WebJob_ImageResample
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage
            (
                [QueueTrigger("bpwebapp-image-resize-queue")] string fileName,
                [Blob("bpwebapp-employee-images/{queueTrigger}", FileAccess.Read)] Stream blobStream,
                [Blob("bpwebapp-employee-images")] CloudBlobContainer container
            )
        {
            blobStream.Seek(0, SeekOrigin.Begin);
            var outputStream = new MemoryStream();
            ResizeImage(blobStream, outputStream);

            var blob = container.GetBlockBlobReference($"{fileName}");

            outputStream.Seek(0, SeekOrigin.Begin);
            blob.UploadFromStream(outputStream);
        }

        private static void ResizeImage(Stream inputStream, Stream outputStream)
        {
            var instructions = new Instructions
            {
                Width = 250,
                Height = 250,
                Mode = FitMode.Carve,
            };
            ImageBuilder.Current.Build(new ImageJob(inputStream, outputStream, instructions));
        }
    }
}
