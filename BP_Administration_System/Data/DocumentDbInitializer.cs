﻿using BP_Administration_System.Models;
using BP_Administration_System.Repositories;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BP_Administration_System.Data
{
    //Trida pro incializaci a naseedovani DocumentDB
    public class DocumentDbInitializer : DocumentDbAdapter
    {
        public DocumentDbInitializer(IOptions<DocumentDbSettings> DocumentDbConfig, ILogger<DocumentDbAdapter> logger) : base(DocumentDbConfig, logger)
        {

        }

        //Inicializacni metoda
        public void Initialize()
        {
            client = new DocumentClient(new Uri(EndpointUri), AuthKey);
            CreateDatabaseIfNotExistsAsync().Wait();
            CreateCollectionIfNotExistsAsync().Wait();
            CreateMeetingCenterDocument().Wait();
            CreateMeetingRoomsDocument().Wait();
            UpdateMeetingCentreRooms().Wait();
            CreateEmployeesDocument().Wait();
        }

        //Pokud databaze neexistuje, tak dojde k jejimu vytvoreni. Pokud exituje, tak dojde k jejimu precteni
        private async Task CreateDatabaseIfNotExistsAsync()
        {
            try
            {
                await client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(DatabaseId));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    await client.CreateDatabaseAsync(new Database { Id = DatabaseId });
                }
                else
                {
                    _logger.LogError("Error in {0}, Error: {1}", nameof(CreateDatabaseIfNotExistsAsync), e.ToString());
                    throw e;
                }
            }
        }

        //Pokud kolekce dane databaze existuje, tak dojde k jejimu precteni.
        //Pokud kolekce neexituje, tak dojde k jejimu vytvoreni v dane databazi, dochazi take k nastaveni propustnosti
        private async Task CreateCollectionIfNotExistsAsync()
        {
            try
            {
                await client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    await client.CreateDocumentCollectionAsync(
                        UriFactory.CreateDatabaseUri(DatabaseId),
                        new DocumentCollection
                        {
                            Id = CollectionId
                        },
                        new RequestOptions { OfferThroughput = 400 });
                }
                else
                {
                    _logger.LogError("Error in {0}, Error: {1}", nameof(CreateCollectionIfNotExistsAsync), e.ToString());
                    throw e;
                }
            }
        }

        //Vytvori Dokument, pokud zadny takovy odpovidajici danemu id v dane kolekci dane databaze neexistuje
        private async Task CreateDocumentIfNotExistsAsync<T>(string DatabaseId, string CollectionId, T entity, string id)
        {
            try
            {
                await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId), entity);
                }
                else
                {
                    _logger.LogError("Error in {0}, Error: {1}", nameof(CreateDocumentIfNotExistsAsync), e.ToString());
                    throw e;
                }
            }
        }

        //Hromadne vytvoreni dokumentu Meeting Centers
        private async Task CreateMeetingCenterDocument()
        {
            foreach (var mc in mcArrayForSeed)
            {
                await CreateDocumentIfNotExistsAsync(DatabaseId, CollectionId, mc, mc.MeetingCenterId);
            }
        }

        //Hromadne vytvoreni dokumentu Meeting Rooms
        private async Task CreateMeetingRoomsDocument()
        {
            foreach (var mr in mrArrayForSeed)
            {
                await CreateDocumentIfNotExistsAsync(DatabaseId, CollectionId, mr, mr.MeetingRoomId);
            }
        }

        //Hromadne vytvoreni dokumentu Employees
        private async Task CreateEmployeesDocument()
        {
            foreach (var employee in employeeArrayForSeed)
            {
                await CreateDocumentIfNotExistsAsync(DatabaseId, CollectionId, employee, employee.EmployeeId);
            }
        }

        #region Seed Meeting Centers, Meeting Room a Employees
        //Metoda pro naseedovani Meeting Center
        private static MeetingCenter[] SeedMeetingCenters()
        {
            return new MeetingCenter[]
            {
                new MeetingCenter{MeetingCenterId = "MC_ID_1", Name = "Code Deluxe Center", Address = "Na Zatlance 28", MeetingRooms = new List<MeetingRoom>(), DocType="MeetingCenter"},
                new MeetingCenter{MeetingCenterId = "MC_ID_2", Name = "PlatinumX Center", Address = "U Pruhonu 12", MeetingRooms = new List<MeetingRoom>(), DocType = "MeetingCenter"},
            };
        }
        private static MeetingCenter[] mcArrayForSeed = SeedMeetingCenters();

        //Metoda pro saseedovani Meeting Roomu
        private static MeetingRoom[] SeedMeetingRooms()
        {
            return new MeetingRoom[]
            {
                new MeetingRoom{MeetingRoomId = "MR_ID_1", Name = "January", Capacity = 3, Description = "Always cold", MeetingCenter = mcArrayForSeed[0], DocType="MeetingRoom"}, //3
                new MeetingRoom{MeetingRoomId = "MR_ID_2", Name = "May", Capacity = 2, Description = "-", MeetingCenter = mcArrayForSeed[0], DocType="MeetingRoom"},  //2
                new MeetingRoom{MeetingRoomId = "MR_ID_3", Name = "Amanda", Capacity = 8, Description = "-", MeetingCenter = mcArrayForSeed[1], DocType="MeetingRoom"},//2
                new MeetingRoom{MeetingRoomId = "MR_ID_4", Name = "Violet", Capacity = 12, Description = "Fresh supplies", MeetingCenter = mcArrayForSeed[1], DocType="MeetingRoom"},//2
                new MeetingRoom{MeetingRoomId = "MR_ID_5", Name = "Siri", Capacity = 10, Description = "Pleasant view", MeetingCenter = mcArrayForSeed[1], DocType="MeetingRoom"},//3
             
            };
        }
        private static MeetingRoom[] mrArrayForSeed = SeedMeetingRooms();

        //Update Meeting Centra, aby nemel Meeting Roomy null
        public async Task ReplaceMeetingCenterDocument(MeetingCenter mc)
        {
            try
            {
                await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, mc.MeetingCenterId), mc);

            }
            catch (DocumentClientException ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(ReplaceMeetingCenterDocument), ex.ToString());
                throw ex;
            }
        }

        //Update konkretniho dokumentu Meeting Centra
        private async Task UpdateMeetingCentreRooms()
        {
            foreach (var meetingCenter in mcArrayForSeed)
            {
                meetingCenter.MeetingRooms.AddRange(mrArrayForSeed.Where(mr => mr.MeetingCenter == meetingCenter));
                await ReplaceMeetingCenterDocument(meetingCenter);
            }
        }

        //Metoda pro naseedovani zamestnancu;
        private static Employee[] SeedEmployees()
        {
            return new Employee[]
             {
                new Employee
                {
                    EmployeeId = "EMP_ID_1",
                    FirstName = "Thomas",
                    LastName = "Mann",
                    Salary = 30000,
                    Age = 28,
                    Tasks = new List<string>{ "Design new system", "Prepare presenation", "" },
                    MeetingCenter = mcArrayForSeed[0],
                    MeetingRoom = mrArrayForSeed[0],
                    DocType = "Employee",
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_2",
                    FirstName = "Rolland",
                    LastName = "Arnold",
                    Salary = 22000,
                    Age = 21,
                    Tasks = new List<string>{ "Analyze Tasks","","" },
                    MeetingCenter = mcArrayForSeed[0],
                    MeetingRoom = mrArrayForSeed[0],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_3",
                    FirstName = "Frank",
                    LastName = "Jones",
                    Salary = 80000,
                    Age = 50,
                    Tasks = new List<string>{ "Management", "Design system", "Talk with customers" },
                    MeetingCenter = mcArrayForSeed[0],
                    MeetingRoom = mrArrayForSeed[0],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_4",
                    FirstName = "Jason",
                    LastName = "Ford",
                    Salary = 50000,
                    Age = 40,
                    Tasks = new List<string>{ "Program system","","" },
                    MeetingCenter = mcArrayForSeed[0],
                    MeetingRoom = mrArrayForSeed[1],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_5",
                    FirstName = "Thomas",
                    LastName = "Stevens",
                    Salary = 100000,
                    Age = 42,
                    Tasks = new List<string>{ "Management",  "Talk with customers", "" },
                    MeetingCenter = mcArrayForSeed[0],
                    MeetingRoom = mrArrayForSeed[1],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_6",
                    FirstName = "Austin",
                    LastName = "Boyd",
                    Salary = 15000,
                    Age = 20,
                    Tasks = new List<string>{ "Accept phone calls", "Prepare coffee", "" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[2],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_7",
                    FirstName = "Jack",
                    LastName = "Nose",
                    Salary = 30000,
                    Age = 22,
                    Tasks = new List<string>{ "Programm system",  "Set up alarm", "" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[2],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_8",
                    FirstName = "Peter",
                    LastName = "Gray",
                    Salary = 48000,
                    Age = 27,
                    Tasks = new List<string>{ "Programm system", "","" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[3],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_9",
                    FirstName = "Harry",
                    LastName = "Dorian",
                    Salary = 50000,
                    Age = 42,
                    Tasks = new List<string>{ "Stay prepared", "Test system", "Talk to managers" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[3],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_10",
                    FirstName = "Bill",
                    LastName = "Travlota",
                    Salary = 85000,
                    Age = 52,
                    Tasks = new List<string>{ "Design architecture", "Talk with customers", "" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[4],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_11",
                    FirstName = "Jarred",
                    LastName = "Toews",
                    Salary = 45000,
                    Age = 36,
                    Tasks = new List<string>{ "Programm system", "Set up alarm", "" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[4],
                    DocType = "Employee"
                },
                new Employee
                {
                    EmployeeId = "EMP_ID_12",
                    FirstName = "Zac",
                    LastName = "Anderson",
                    Salary = 36000,
                    Age = 30,
                    Tasks = new List<string>{ "Analyze system", "Prepare presentation", "" },
                    MeetingCenter = mcArrayForSeed[1],
                    MeetingRoom = mrArrayForSeed[4],
                    DocType = "Employee"
                },
             };
        }
        private static Employee[] employeeArrayForSeed = SeedEmployees();
        #endregion
    }
}
