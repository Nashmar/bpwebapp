﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Models.TableStorage
{
    public class ProfileSettings : TableEntity
    {
        public ProfileSettings()
        {

        }

        public ProfileSettings(string employeeId, string profileSettingsId)
        {
            this.PartitionKey = employeeId;
            this.RowKey = profileSettingsId;
        }

        [StringLength(15, MinimumLength = 1), RegularExpression(@"^[A-Za-z0-9\s]+$",
            ErrorMessage = "The field Nickname must contain only letters and numbers")]
        public string Nickname { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [RegularExpression(@"\b(Yes|No|\s)\b$",
            ErrorMessage = "The field Avatar must be one of strings Yes or No")]
        public string Newsletter { get; set; }
    }
}
