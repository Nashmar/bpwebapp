﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BP_Administration_System.Models.TableStorage;
using BP_Administration_System_Develop.Repositories.BlobStorage;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace BP_Administration_System.Repositories.BlobStorage
{
    //Repozitar zakladnich operaci s BlobStorage
    public class BlobStorageRepository : BlobStorageAdapter, IBlobStorageRepository
    {
        public BlobStorageRepository(IOptions<StorageSettings> BlobStorageConfig, string specificContainer, ILogger<BlobStorageAdapter> logger)
            : base(BlobStorageConfig, specificContainer, logger)
        {

        }

        //Ziskani vsech blobu ulozenych v danem containeru
        public async Task<IEnumerable<CloudBlockBlob>> GetAllBlobsAsync()
        {
            BlobContinuationToken blobContinuationToken = null;
            List<CloudBlockBlob> results = new List<CloudBlockBlob>();

            do
            {
                BlobResultSegment resultSegment = await container.ListBlobsSegmentedAsync(null, blobContinuationToken);
                blobContinuationToken = resultSegment.ContinuationToken;

                foreach (var item in resultSegment.Results)
                {
                    results.Add((CloudBlockBlob)item);
                }

            } while (blobContinuationToken != null);

            return results;
        }

        //Ziskani specifickeho blobu podle zadaneho nazvu blobu
        public CloudBlockBlob GetBlob(string blobName)
        {
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
            return blockBlob;
        }

        //Nahrani specifickeho media do blobu
        public async Task<CloudBlockBlob> UploadBlobAsync(IFormFile file)
        {
            try
            {
                var fileName = Path.GetFileName(file.FileName);
                string blobName = string.Format("{0}-{1}", Guid.NewGuid().ToString(), fileName);
                CloudBlockBlob blob = container.GetBlockBlobReference(blobName);

                using (var fileStream = file.OpenReadStream())
                {
                    await blob.UploadFromStreamAsync(fileStream);
                }

                await blob.FetchAttributesAsync();
                return blob;
            }

            catch (Exception ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(UploadBlobAsync), ex.ToString());
                throw ex;
            }
        }


        //Stazeni specifickeho blobu podle zadaneho nazvu blobu
        public async Task<MemoryStream> DownloadBlobAsync(string blobName)
        {
            try
            {
                CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
                using (var stream = new MemoryStream())
                {
                    await blob.DownloadToStreamAsync(stream);
                    return stream;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(DownloadBlobAsync), ex.ToString());
                throw ex;
            }

        }

        //Smazani specifickeho blobu podle zadaneho nazvu blobu z Containeru
        public async Task DeleteBlobAsync(string blobName)
        {
            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
            await blob.DeleteAsync(); ;
        }
    }
}
