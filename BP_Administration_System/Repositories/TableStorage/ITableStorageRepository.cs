﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.TableStorage
{
    //Interface zakladnich CRUD operaci
    public interface ITableStorageRepository<TModel>
    {
        Task<IEnumerable<TModel>> GetEntitiesAsync();
        Task<TModel> GetEntityAsync(string partitionKey, string rowKey);
        Task InsertEntityAsync(string partitionKey);
        Task UpdateEntityAsync(TModel entity);
        Task DeleteEntityAsync(string partitionKey, string rowKey);
    }
}
