﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BP_Administration_System.Models;
using BP_Administration_System.Models.TableStorage;
using BP_Administration_System.Repositories;
using BP_Administration_System.Repositories.BlobStorage;
using BP_Administration_System.Repositories.QueueStorage;
using BP_Administration_System.Repositories.TableStorage;
using BP_Administration_System_Develop.Repositories.BlobStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BP_Administration_System
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Registrace nastaveni DocumentDB
            services.Configure<DocumentDbSettings>(Configuration.GetSection("DocumentDbConfig"));
            //Registrace nastaveni Azure Storage
            services.Configure<StorageSettings>(Configuration.GetSection("AzureStorageConfig"));
            //Registrace services souvisejicich s DocumentDB
            services.AddTransient<IEmployeeDocumentDbRepository, EmployeeRepository>();
            services.AddTransient<IMeetingCenterDocumentDbRepository, MeetingCenterRepository>();
            services.AddTransient<IDocumentDbRepository<MeetingRoom, string>, MeetingRoomRepository>();
            //Registrace services souvisejicich s TableStorage
            services.AddTransient<ITableStorageRepository<ApplicationSettings>, ApplicationSettingsRepository>();
            services.AddTransient<ITableStorageRepository<ProfileSettings>, ProfileSettingsRepository>();
            //Registrace services souvisejicich s BlobStorage
            services.AddTransient<IPhotosRepository, PhotosRepository>();
            services.AddTransient<IVideosRepository, VideosRepository>();
            //Registrace services souvisejicich s QueueStorage
            services.AddTransient<IQueueStorageRepository, QueueStorageRepository>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddDebug();
            loggerFactory.AddAzureWebAppDiagnostics();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
