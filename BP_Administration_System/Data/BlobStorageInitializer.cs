﻿using BP_Administration_System.Models;
using BP_Administration_System.Models.TableStorage;
using BP_Administration_System_Develop.Repositories.BlobStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BP_Administration_System.Data
{
    //Trida pro inicializaci a naseedovani Blob Storage
    public class BlobStorageInitializer : BlobStorageAdapter
    {
        private CloudBlobContainer videosContainer;

        public BlobStorageInitializer(IOptions<StorageSettings> BlobStorageConfig, ILogger<BlobStorageAdapter> logger)
            : base(BlobStorageConfig, BlobStorageConfig.Value.PhotosContainerName, logger)
        {
            this.videosContainer = blobClient.GetContainerReference(BlobStorageConfig.Value.VideosContainerName);
        }

        //Inicializacni metoda
        public void Initialize()
        {
            CreateContainersAsync().Wait();
            UploadBlobs().Wait();
        }

        //Vytvoreni Containeru
        public async Task CreateContainersAsync()
        {
            try
            {
                await container.CreateIfNotExistsAsync();
                await videosContainer.CreateIfNotExistsAsync();
            }
            catch (StorageException ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(CreateContainersAsync), ex.ToString());
                throw ex;
            }
            BlobContainerPermissions permissions = new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            };
            await container.SetPermissionsAsync(permissions);
            await videosContainer.SetPermissionsAsync(permissions);
        }

        //Ziskani vsech cest k souborum fotek zamestnancu v aplikaci
        public List<string> GetAllPhotoFilesPaths()
        {
            try
            {
                string path = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @".\wwwroot\images"));

                var filePaths = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories)
                .Where(s => s.EndsWith(".png") || s.EndsWith(".jpg") || s.EndsWith(".jpeg"));

                List<string> allImagesFilePaths = new List<string>();
                allImagesFilePaths.AddRange(filePaths);
                return allImagesFilePaths;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(GetAllPhotoFilesPaths), ex.ToString());
                throw ex;
            }
        }

        //Nahrani vsech obrazku do Blob Storage
        public async Task UploadBlobs()
        {
            List<string> BlobNamesGuid = new List<string>
            {
                "6cad7553-7f15-4491-a8b2-593637f4f3c0",
                "350c5ab3-121a-4776-9170-913434e8fc4b",
                "1ccffda8-6ba0-400e-96ce-4e80bee7f59c",
                "8304309a-1ce2-4179-811a-fbc7657f616a",
                "0c1178e4-e955-4290-8dee-8a911ab89f88",
                "6cd08037-ffd8-4371-9204-95c3ac342bff"
            };

            CloudBlockBlob blob;
            int index = 0;
            try
            {
                foreach (var filePath in GetAllPhotoFilesPaths())
                {

                    var fileName = Path.GetFileName(filePath);
                    string blobName = string.Format("{0}-{1}", BlobNamesGuid[index], fileName);
                    blob = container.GetBlockBlobReference(blobName);
                    using (var fileStream = File.OpenRead(filePath))
                    {
                        await blob.UploadFromStreamAsync(fileStream);
                    }
                    index++;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(UploadBlobs), ex.ToString());
                throw ex;
            }
        }
    }
}
