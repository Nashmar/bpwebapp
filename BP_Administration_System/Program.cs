﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BP_Administration_System.Data;
using BP_Administration_System.Models;
using BP_Administration_System.Models.TableStorage;
using BP_Administration_System.Repositories;
using BP_Administration_System.Repositories.QueueStorage;
using BP_Administration_System.Repositories.TableStorage;
using BP_Administration_System_Develop.Repositories.BlobStorage;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BP_Administration_System
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    //DocumentDB
                    var DocumentDbConfig = services.GetRequiredService<IOptions<DocumentDbSettings>>();
                    var LoggerDocDB = services.GetRequiredService<ILogger<DocumentDbAdapter>>();
                    DocumentDbInitializer dbInitalizer = new DocumentDbInitializer(DocumentDbConfig, LoggerDocDB);
                    dbInitalizer.Initialize(); 

                    //Table Storage 
                    var StorageConfig = services.GetRequiredService<IOptions<StorageSettings>>();
                    var LoggerTS = services.GetRequiredService<ILogger<TableStorageAdapter>>();
                    TableStorageInitializer tableInitalizer = new TableStorageInitializer(StorageConfig, LoggerTS);
                    var employees = services.GetRequiredService<IDocumentDbRepository<Employee, string>>().GetItemsAsync().Result;
                    tableInitalizer.Initialize(employees);

                    //QueueStorage
                    var LoggerQS = services.GetRequiredService<ILogger<QueueStorageAdapter>>();
                    QueueStorageInitializer queueInitializer = new QueueStorageInitializer(StorageConfig, LoggerQS);
                    queueInitializer.Initialize();

                    //Blob Storage
                    var LoggerBS = services.GetRequiredService<ILogger<BlobStorageAdapter>>();
                    BlobStorageInitializer blobInitalizer = new BlobStorageInitializer(StorageConfig, LoggerBS);
                    blobInitalizer.Initialize();
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding database.");
                }
            }
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
