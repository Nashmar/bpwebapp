﻿using BP_Administration_System.Models.TableStorage;
using BP_Administration_System.Repositories.BlobStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System_Develop.Repositories.BlobStorage
{
    //Interface specifickych operaci pro Photos
    public interface IPhotosRepository : IBlobStorageRepository
    {

    }

    //Repozitar pro praci s Photos
    public class PhotosRepository : BlobStorageRepository, IPhotosRepository
    {
        public PhotosRepository(IOptions<StorageSettings> BlobStorageConfig, ILogger<BlobStorageAdapter> logger)
            : base(BlobStorageConfig, BlobStorageConfig.Value.PhotosContainerName, logger)
        {

        }
    }
}
