﻿using BP_Administration_System.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Repozitar zakladnich CRUD operaci pro MeetingRoomy
    public class MeetingRoomRepository : DocumentDbAdapter, IDocumentDbRepository<MeetingRoom, string>
    {
        public MeetingRoomRepository(IOptions<DocumentDbSettings> DocumentDbConfig, ILogger<DocumentDbAdapter> logger) : base(DocumentDbConfig, logger)
        {

        }

        //Precteni vsech dokumentu typu MeetingRoom
        public async Task<IEnumerable<MeetingRoom>> GetItemsAsync()
        {
            IDocumentQuery<MeetingRoom> query = client.CreateDocumentQuery<MeetingRoom>(
            UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId))
            .Where(mr => mr.DocType == nameof(MeetingRoom))
            .AsDocumentQuery();

            List<MeetingRoom> results = new List<MeetingRoom>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<MeetingRoom>());
            }
            return results;
        }

        //Precteni konkretniho dokumentu typu MeetingRoom na zaklade pozadovaneho id
        public async Task<MeetingRoom> GetItemAsync(string id)
        {
            try
            {
                Document document = await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
                return (MeetingRoom)(dynamic)document;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("Cannot GET Meeting Room with id {0} in {1}", id, nameof(GetItemAsync));
                    return null;
                }
                else
                {
                    _logger.LogError("Error in {0}, Error: {1}", nameof(GetItemAsync), e.ToString());
                    throw e;
                }
            }
        }

        //Methods not needed in prototype
        #region Zatim nepotrebne metody v prototypu
        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public Task<MeetingRoom> UpdateDocumentAsync(string id, MeetingRoom item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteDocumentAsync(string id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Task<MeetingRoom> AddDocumentAsync(MeetingRoom item)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}


