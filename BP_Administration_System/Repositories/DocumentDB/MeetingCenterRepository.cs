﻿using BP_Administration_System.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Repozitar zakladnich CRUD operaci pro MeetingCentra
    public class MeetingCenterRepository : DocumentDbAdapter, IMeetingCenterDocumentDbRepository
    {
        public MeetingCenterRepository(IOptions<DocumentDbSettings> DocumentDbConfig, ILogger<DocumentDbAdapter> logger) : base(DocumentDbConfig, logger)
        {
        }

        //Precteni vsech dokumentu typu MeetingCenter
        public async Task<IEnumerable<MeetingCenter>> GetItemsAsync()
        {
            IDocumentQuery<MeetingCenter> query = client.CreateDocumentQuery<MeetingCenter>(
            UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId))
            .Where(mc => mc.DocType == nameof(MeetingCenter))
            .AsDocumentQuery();

            List<MeetingCenter> results = new List<MeetingCenter>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<MeetingCenter>());
            }
            return results;
        }

        //Precteni konkretniho dokumentu typu MeetingCenter na zaklade pozadovaneho id
        public async Task<MeetingCenter> GetItemAsync(string id)
        {
            try
            {
                Document document = await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
                return (MeetingCenter)(dynamic)document;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("Cannot GET Meeting Center with id {0} in {1}", id, nameof(GetItemAsync));
                    return null;
                }
                else
                {
                    _logger.LogError("Error in {0}, Error: {1}", nameof(GetItemAsync), e.ToString());
                    throw e;
                }
            }
        }

        //Ziskani specifickeho Meeting Roomu jako Slect List Item
        public IEnumerable<SelectListItem> GetSpecificMeetingRoom(MeetingCenter meetingCenter)
        {
            var AllMeetingRooms = meetingCenter.MeetingRooms;
            var specificMeetingRooms = AllMeetingRooms.Where(mr => mr.MeetingCenter.MeetingCenterId == meetingCenter.MeetingCenterId);

            return specificMeetingRooms.Select(mr => new SelectListItem()
            {
                Value = mr.MeetingRoomId,
                Text = mr.Name
            });
        }
        //Methods not needed in prototype
        #region Zatim nepotrebne metody v prototypu
        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public Task<MeetingCenter> UpdateDocumentAsync(string id, MeetingCenter item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteDocumentAsync(string id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Task<MeetingCenter> AddDocumentAsync(MeetingCenter item)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
