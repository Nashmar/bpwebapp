﻿using BP_Administration_System.Data;
using BP_Administration_System.Models;
using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.TableStorage
{
    //Repozitar zakladnich CRUD operaci pro ApplicationSettings
    public class ApplicationSettingsRepository : TableStorageAdapter, ITableStorageRepository<ApplicationSettings>
    {
        public ApplicationSettingsRepository(IOptions<StorageSettings> TableStorageConfig, ILogger<TableStorageAdapter> logger) : base(TableStorageConfig, logger)
        {

        }

        //Ziskani vsech entit ApplicationSettings v tabulce
        public async Task<IEnumerable<ApplicationSettings>> GetEntitiesAsync()
        {
            TableQuery<ApplicationSettings> query = new TableQuery<ApplicationSettings>()
                 .Where(TableQuery.GenerateFilterCondition("RowKey",
                                        QueryComparisons.Equal, "ApplicationSettings"));


            List<ApplicationSettings> results = new List<ApplicationSettings>();
            TableContinuationToken continuationToken = null;
            do
            {
                TableQuerySegment<ApplicationSettings> queryResults =
                    await table.ExecuteQuerySegmentedAsync(query, continuationToken);

                continuationToken = queryResults.ContinuationToken;

                results.AddRange(queryResults.Results);

            } while (continuationToken != null);

            return results;
        }

        //Ziskani prisluse Entity odpovidajici zadanemu PartitionKey a RowKey
        public async Task<ApplicationSettings> GetEntityAsync(string partitionKey, string rowKey)
        {
            TableOperation operation = TableOperation.Retrieve<ApplicationSettings>(partitionKey, rowKey);
            TableResult result = await table.ExecuteAsync(operation);

            return (ApplicationSettings)(dynamic)result.Result;
        }

        //Vlozeni Entitu se specifikovanymy vlastnostmi do tabulky
        public async Task InsertEntityAsync(string partitionKey)
        {
            ApplicationSettings entity = new ApplicationSettings(partitionKey, "ApplicationSettings") { DesignTheme = "Light", Language = "EN" };

            TableOperation operation = TableOperation.Insert(entity);
            await table.ExecuteAsync(operation);
        }

        //Aktualizace dane Entity
        public async Task UpdateEntityAsync(ApplicationSettings entity)
        {
            TableOperation operation = TableOperation.InsertOrReplace(entity);
            await table.ExecuteAsync(operation);
        }

        //Smazani dane Entity podle hodnot PartitionKey a RowKey
        public async Task DeleteEntityAsync(string partitionKey, string rowKey)
        {
            ApplicationSettings entity = await GetEntityAsync(partitionKey, rowKey);
            TableOperation operation = TableOperation.Delete(entity);

            await table.ExecuteAsync(operation);
        }
    }
}
