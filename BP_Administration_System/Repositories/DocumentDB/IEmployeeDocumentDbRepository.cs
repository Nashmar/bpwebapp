﻿using BP_Administration_System.Models;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Interface specifickych operaci pro Employees
    public interface IEmployeeDocumentDbRepository : IDocumentDbRepository<Employee, string>
    {
        int GetActualCapacityOfRoom(IEnumerable<Employee> employees, MeetingRoom meetingRoom);
        IEnumerable<Employee> SortBy<T>(IEnumerable<Employee> employees, Func<Employee, T> condition);
    }
}
