﻿using BP_Administration_System.Models;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.ViewModels
{
    //ViewModel pro praci s Bloby
    public class BlobsViewModel
    {
        public IEnumerable<CloudBlockBlob> photoBlobs { get; set; }
        public IEnumerable<CloudBlockBlob> videoBlobs { get; set; }
        public CloudBlockBlob blob { get; set; }
    }
}
