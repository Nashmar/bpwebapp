﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Models.TableStorage
{
    //Model pro nastaveni Azure Storage
    public class StorageSettings
    {
        public string AzureStorageConnectionString { get; set; }
        public string TableName { get; set; }
        public string PhotosContainerName { get; set; }
        public string VideosContainerName { get; set; }
        public string QueueName { get; set; }
    }
}
