﻿using BP_Administration_System.Models.TableStorage;
using BP_Administration_System_Develop.Repositories.BlobStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.BlobStorage
{
    //Interface specifickych operaci pro Videos
    public interface IVideosRepository : IBlobStorageRepository
    {

    }

    //Repozitar pro praci s Videos
    public class VideosRepository : BlobStorageRepository, IVideosRepository
    {
        public VideosRepository(IOptions<StorageSettings> BlobStorageConfig, ILogger<BlobStorageAdapter> logger)
            : base(BlobStorageConfig, BlobStorageConfig.Value.VideosContainerName, logger)
        {

        }
    }
}
