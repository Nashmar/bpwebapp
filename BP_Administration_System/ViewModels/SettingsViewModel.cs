﻿using BP_Administration_System.Models.TableStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.ViewModels
{
    //ViewModel pro praci s uzivatelskym nastavenim
    public class SettingsViewModel
    {
        public IEnumerable<ApplicationSettings> applicationSettings { get; set; }
        public IEnumerable<ProfileSettings> profileSettings { get; set; }
    }
}
