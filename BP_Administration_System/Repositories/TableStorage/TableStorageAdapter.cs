﻿using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.TableStorage
{
    //Abstraktni trida pro komunikaci s Table Storage
    public abstract class TableStorageAdapter
    {
        protected readonly string azureStorageConnectionString;
        protected CloudTable table;
        protected CloudTableClient tableClient;
        protected CloudStorageAccount storageAccount;
        protected readonly ILogger _logger;

        public TableStorageAdapter(IOptions<StorageSettings> TableStorageConfig, ILogger<TableStorageAdapter> logger)
        {
            this.azureStorageConnectionString = TableStorageConfig.Value.AzureStorageConnectionString;
            storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);
            tableClient = storageAccount.CreateCloudTableClient();
            this.table = tableClient.GetTableReference(TableStorageConfig.Value.TableName);
            _logger = logger;
        }
    }
}
