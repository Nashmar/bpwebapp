﻿using BP_Administration_System.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories
{
    //Interface specifickych operaci pro MeetingCentra
    public interface IMeetingCenterDocumentDbRepository : IDocumentDbRepository<MeetingCenter, string>
    {
        IEnumerable<SelectListItem> GetSpecificMeetingRoom(MeetingCenter meetingCenter);
    }
}

