﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BP_Administration_System.Models;
using BP_Administration_System.Models.TableStorage;
using BP_Administration_System.Repositories;
using BP_Administration_System.Repositories.TableStorage;
using BP_Administration_System.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BP_Administration_System.Controllers
{
    public class EmployeeSettingsController : Controller
    {
        private readonly ITableStorageRepository<ApplicationSettings> _appSettingsRepository;
        private readonly ITableStorageRepository<ProfileSettings> _profileSettingsRepository;
        private readonly ILogger _logger;

        public EmployeeSettingsController
            (
            ITableStorageRepository<ApplicationSettings> appSettingsRepository,
            ITableStorageRepository<ProfileSettings> profileSettingsRepository,
            ILogger<EmployeeSettingsController> logger
            )
        {
            _appSettingsRepository = appSettingsRepository;
            _profileSettingsRepository = profileSettingsRepository;
            _logger = logger;
        }

        //Index EmployeeSettings
        public async Task<IActionResult> AllEmployeeSettings()
        {
            return View(new SettingsViewModel
            {
                applicationSettings = await _appSettingsRepository.GetEntitiesAsync(),
                profileSettings = await _profileSettingsRepository.GetEntitiesAsync()
            });
        }

        //Edit AppSettings GET
        public async Task<IActionResult> EditAppSettings(string partitionKey, string rowKey)
        {
            _logger.LogInformation("Editing Application Settings in {0}", nameof(EditAppSettings));

            if (partitionKey == null)
            {
                _logger.LogWarning("Partition Key {0} in {1} NOT FOUND", partitionKey, nameof(EditAppSettings));
                return NotFound();
            }
            ApplicationSettings appSettings = await _appSettingsRepository.GetEntityAsync(partitionKey, rowKey);

            if (appSettings == null)
            {
                _logger.LogWarning("Application Settings{0} in {1} NOT FOUND", appSettings, nameof(EditAppSettings));
                return NotFound();
            }

            return View(appSettings);
        }

        //Edit AppSettings POST
        [HttpPost]
        public async Task<IActionResult> EditAppSettings(ApplicationSettings appSettings)
        {
            if (ModelState.IsValid)
            {
                await _appSettingsRepository.UpdateEntityAsync(appSettings);

                TempData["noticeUpdated"] = "Application Settings were successfuly updated";
                return RedirectToAction("AllEmployeeSettings");
            }
            _logger.LogWarning("Application Settings {0} in {1} IS NOT VALID", appSettings, nameof(EditAppSettings));
            return View();
        }

        //Edit ProfileSettings GET
        public async Task<IActionResult> EditProfileSettings(string partitionKey, string rowKey)
        {
            _logger.LogInformation("Editing Profile Settings in {0}", nameof(EditProfileSettings));
            if (partitionKey == null)
            {
                _logger.LogWarning("Partition Key {0} in {1} NOT FOUND", partitionKey, nameof(EditProfileSettings));
                return NotFound();
            }
            ProfileSettings profileSettings = await _profileSettingsRepository.GetEntityAsync(partitionKey, rowKey);

            if (profileSettings == null)
            {
                _logger.LogWarning("Profile Settings{0} in {1} NOT FOUND", profileSettings, nameof(EditProfileSettings));
                return NotFound();
            }

            return View(profileSettings);
        }

        //Edit ProfileSettings POST
        [HttpPost]
        public async Task<IActionResult> EditProfileSettings(ProfileSettings profileSettings)
        {
            if (ModelState.IsValid)
            {
                await _profileSettingsRepository.UpdateEntityAsync(profileSettings);

                TempData["noticeUpdated"] = "Profile Settings were successfuly updated";
                return RedirectToAction("AllEmployeeSettings");
            }
            _logger.LogWarning("Profile Settings {0} in {1} IS NOT VALID", profileSettings, nameof(EditProfileSettings));
            return View();
        }
    }
}