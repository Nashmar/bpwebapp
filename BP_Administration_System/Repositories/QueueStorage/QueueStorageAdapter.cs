﻿using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.QueueStorage
{
    //Abstraktni trida pro komunikaci s Queue Storage
    public abstract class QueueStorageAdapter
    {
        protected readonly string azureStorageConnectionString;
        protected CloudQueue queue;
        protected CloudQueueClient queueClient;
        protected CloudStorageAccount storageAccount;
        protected readonly ILogger _logger;

        public QueueStorageAdapter(IOptions<StorageSettings> QueueStorageConfig,ILogger<QueueStorageAdapter> logger)
        {
            this.azureStorageConnectionString = QueueStorageConfig.Value.AzureStorageConnectionString;
            storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);
            queueClient = storageAccount.CreateCloudQueueClient();
            this.queue = queueClient.GetQueueReference(QueueStorageConfig.Value.QueueName);
            _logger = logger;
        }
    }
}
