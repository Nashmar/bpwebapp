﻿using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.TableStorage
{
    //Repozitar zakladnich CRUD operaci pro ProfileSettings
    public class ProfileSettingsRepository : TableStorageAdapter, ITableStorageRepository<ProfileSettings>
    {
        public ProfileSettingsRepository(IOptions<StorageSettings> TableStorageConfig, ILogger<TableStorageAdapter> logger) : base(TableStorageConfig, logger)
        {

        }

        //Ziskani vsech entit ProfileSettings v tabulce
        public async Task<IEnumerable<ProfileSettings>> GetEntitiesAsync()
        {
            TableQuery<ProfileSettings> query = new TableQuery<ProfileSettings>()
                 .Where(TableQuery.GenerateFilterCondition("RowKey",
                                        QueryComparisons.Equal, "ProfileSettings"));

            List<ProfileSettings> results = new List<ProfileSettings>();
            TableContinuationToken continuationToken = null;
            do
            {
                TableQuerySegment<ProfileSettings> queryResults =
                    await table.ExecuteQuerySegmentedAsync(query, continuationToken);

                continuationToken = queryResults.ContinuationToken;

                results.AddRange(queryResults.Results);

            } while (continuationToken != null);

            return results;
        }

        //Ziskani prisluse Entity odpovidajici zadanemu PartitionKey a RowKey
        public async Task<ProfileSettings> GetEntityAsync(string partitionKey, string rowKey)
        {
            TableOperation operation = TableOperation.Retrieve<ProfileSettings>(partitionKey, rowKey);

            TableResult result = await table.ExecuteAsync(operation);

            return (ProfileSettings)(dynamic)result.Result;
        }

        //Vlozeni Entitu se specifikovanymy vlastnostmi do tabulky
        public async Task InsertEntityAsync(string partitionKey)
        {
            ProfileSettings entity = new ProfileSettings(partitionKey, "ProfileSettings")
            { Nickname = "", Email = "defaul@default.com", Newsletter = "No" };

            TableOperation operation = TableOperation.Insert(entity);
            await table.ExecuteAsync(operation);
        }

        //Aktualizace dane Entity 
        public async Task UpdateEntityAsync(ProfileSettings entity)
        {
            TableOperation operation = TableOperation.InsertOrReplace(entity);
            await table.ExecuteAsync(operation);
        }

        //Smazani dane Entity podle hodnot PartitionKey a RowKey
        public async Task DeleteEntityAsync(string partitionKey, string rowKey)
        {
            ProfileSettings entity = await GetEntityAsync(partitionKey, rowKey);

            TableOperation operation = TableOperation.Delete(entity);

            await table.ExecuteAsync(operation);
        }
    }
}
