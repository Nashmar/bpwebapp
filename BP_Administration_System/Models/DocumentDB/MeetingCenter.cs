﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Models
{
    [JsonObject(IsReference = true)]
    public class MeetingCenter
    {
        [JsonProperty(PropertyName = "id")]
        public string MeetingCenterId { get; set; }

        [JsonProperty(PropertyName = "name")]
        [Display(Name = "Meeting Center Name"), StringLength(30, MinimumLength = 1)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "address")]
        [Display(Name = "Address"), StringLength(50, MinimumLength = 1)]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "meetingRooms")]
        [Display(Name = "Meeting Rooms")]
        public List<MeetingRoom> MeetingRooms { get; set; }

        [Display(Name = "Meeting Rooms")]
        public string MeetingRoomsCommaSeperated
        {
            get { if (MeetingRooms != null) { return String.Join(", ", MeetingRooms.Select(x => x.Name)); } return ""; }
        }

        [JsonProperty(PropertyName = "docType")]
        public string DocType { get; set; }
    }
}
