##Administration System - Overview

Practical part of my bachelor's thesis - a prototype of a fictional company's administration system. 

###Azure Services

This prototype of web application is integrated with selected services of cloud platform Microsoft Azure such as: 

* Azure App Services

* Azure Blob Storage

* Azure Table Storage

* Azure Queue Storage

* Azure DocumentDB

###Technologies

Web application is programmed with programming language C# and based on ASP.NET Core MVC framework (.NET Core v2.0). 

Views make use of technologies such as HTML, CSS, jQuery and Bootstrap framework for responsive web design.

###Description
Web application is deployed in Microsoft Azure and is available from: https://bpwebapplication.azurewebsites.net/

The company owns several buildings known as Meeting Centers, where meeting rooms are known as Meeting Rooms. These Meeting Rooms are used by company employees. In the administration system, there is a database of employees in which you can view and filter the entire list of company employees. It is also possible to create new employees or to display details, update or delete existing employees. Each employee in the app also has some custom settings. This is the user profile category as well as the application settings category. The user's custom setting is directly dependent on the employee. If a new employee is created in the application, their user settings are automatically created. This setting can then be viewed and updated in the Employee Settings section. If the employee drops out of the app, its user settings are also deleted. The last part of the application is a media repository. This section contains a list of employee images and a list of uploaded videos. For images, you can view their details or download or delete individual images. It is also possible to upload images of the employees in any resolution into the application. After uploading an image to an application, it is asynchronously processed to the desired resolution. In the case of videos, it is possible to upload individual videos to the application or to download or delete them from the application.

###Usage of Microsoft Azure services in application

* Azure Blob Storage - Is typically used to store a wide range of unstructured data. In the application, Azure Blob Storage is used as a storage for images that can be embedded in an application when creating employee photos or as a video storage and storage for logs.

* Azure Table Storage - Is suitable for storing large amounts of data that need to be accessed frequently and quickly, but which do not need to be queried. In the application, this storage is used for the various user settings available to each user of the application. This is the user profile category and the application settings category.

* Azure Queue Storage - When an image is saved in the application in Blob Storage, a message containing the relevant image information is created and queued in the application. From the queue, this message picks up the WebJob and, on the basis of it, handles that background image, so there will be no delay in using the application. Once the processing is completed, the new version of the image is saved to Blob Storage and the message is deleted from the queue. Asynchronous image processing aims to compress each embedded image and create a thumbnail corresponding to its resolution.

* Azure DocumentDB - Provides quick access to data and the ability to query these data. In the application, this service has been selected as the appropriate storage for created employees, meeting centers, and meeting rooms.