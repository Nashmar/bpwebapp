﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BP_Administration_System.Models;
using BP_Administration_System.Repositories;
using Microsoft.AspNetCore.Http;
using BP_Administration_System.Repositories.TableStorage;
using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;

namespace BP_Administration_System.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDocumentDbRepository<MeetingRoom, string> _meetingRoomRepository;
        private readonly IEmployeeDocumentDbRepository _empOperationsRepository;
        private readonly IMeetingCenterDocumentDbRepository _mcOperationsRepository;
        private readonly ITableStorageRepository<ApplicationSettings> _appSettingsRepository;
        private readonly ITableStorageRepository<ProfileSettings> _profileSettingsRepository;
        private readonly ILogger _logger;
        private static string OldLastNameValue;

        public HomeController
            (
            IDocumentDbRepository<MeetingRoom, string> meetingRoom,
            IMeetingCenterDocumentDbRepository mcOperationsRepository,
            IEmployeeDocumentDbRepository empOperationsRepository,
            ITableStorageRepository<ApplicationSettings> appSettingsRepository,
            ITableStorageRepository<ProfileSettings> profileSettingsRepository,
            ILogger<HomeController> logger
            )
        {
            _meetingRoomRepository = meetingRoom;
            _mcOperationsRepository = mcOperationsRepository;
            _empOperationsRepository = empOperationsRepository;
            _appSettingsRepository = appSettingsRepository;
            _profileSettingsRepository = profileSettingsRepository;
            _logger = logger;
        }

        //Index aplikace GET
        public async Task<IActionResult> Index()
        {
            return View(await _empOperationsRepository.GetItemsAsync());
        }

        //Detail Employee GET
        public async Task<IActionResult> EmployeeDetails(string id)
        {
            _logger.LogInformation("Getting employee with id {0} in {1}", id, nameof(EmployeeDetails));
            if (id == null)
            {
                _logger.LogWarning("Detail of employee with id {0} in {1} NOT FOUND", id, nameof(EmployeeDetails));
                return NotFound();
            }
            return View(await _empOperationsRepository.GetItemAsync(id));
        }

        //Delete Employee POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            _logger.LogInformation("Deleting employee with id {0} in {1}", id, nameof(DeleteAsync));
            if (id == null)
            {
                _logger.LogWarning("Cannot delete employee with id {0} in {1} - NOT FOUND", id, nameof(DeleteAsync));
                return NotFound();
            }

            var employee = await _empOperationsRepository.GetItemAsync(id);
            await _profileSettingsRepository.DeleteEntityAsync(employee.LastName, "ProfileSettings"); //TableStorage
            await _appSettingsRepository.DeleteEntityAsync(employee.LastName, "ApplicationSettings"); //TableStorage

            await _empOperationsRepository.DeleteDocumentAsync(id);

            TempData["noticeDeleted"] = "Employee was successfuly deleted";
            return RedirectToAction("Index");
        }

        //Create Employee GET
        [ActionName("Create")]
        public async Task<IActionResult> CreateAsync()
        {
            _logger.LogInformation("Creating employee in {0}", nameof(CreateAsync));
            var selectedMeetingCenter = await _mcOperationsRepository.GetItemAsync("MC_ID_2");
            ViewBag.MeetingCenterId = selectedMeetingCenter.MeetingCenterId;
            ViewBag.MeetingCenterName = selectedMeetingCenter.Name;
            ViewBag.MeetingRoomsList = _mcOperationsRepository.GetSpecificMeetingRoom(selectedMeetingCenter);
            return View();
        }

        //Create Employee POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Create")]
        public async Task<IActionResult> CreateAsync(Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.MeetingCenter = await _mcOperationsRepository.GetItemAsync("MC_ID_2");

                var selectedMeetingRoomId = employee.MeetingRoom.MeetingRoomId;
                employee.MeetingRoom = await _meetingRoomRepository.GetItemAsync(selectedMeetingRoomId);

                await _empOperationsRepository.AddDocumentAsync(employee);

                await _profileSettingsRepository.InsertEntityAsync(employee.LastName);  //Table Storage
                await _appSettingsRepository.InsertEntityAsync(employee.LastName); //Table Storage

                TempData["noticeCreated"] = "Employee was successfuly created";
                return RedirectToAction("Index");
            }
            _logger.LogWarning("Employee in {0} IS NOT VALID", nameof(CreateAsync));
            return View();
        }

        //Edit Employee GET
        [ActionName("Edit")]
        public async Task<IActionResult> EditAsync(string id)
        {
            _logger.LogInformation("Editing employee  with id {0} in {1}", id, nameof(EditAsync));
            if (id == null)
            {
                _logger.LogWarning("Id of employee {0} in {1} NOT FOUND", id, nameof(EditAsync));
                return NotFound();
            }

            Employee employee = await _empOperationsRepository.GetItemAsync(id);
            OldLastNameValue = employee.LastName;
            if (employee == null)
            {
                _logger.LogWarning("Employee in {0} NOT FOUND", nameof(EditAsync));
                return NotFound();
            }
            var selectedMeetingCenter = await _mcOperationsRepository.GetItemAsync("MC_ID_2");
            ViewBag.MeetingCenterId = selectedMeetingCenter.MeetingCenterId;
            ViewBag.MeetingCenterName = selectedMeetingCenter.Name;
            ViewBag.MeetingRoomsList = _mcOperationsRepository.GetSpecificMeetingRoom(selectedMeetingCenter);

            return View(employee);
        }

        //Edit Employee POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Edit")]
        public async Task<IActionResult> EditAsync(string id, Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.MeetingCenter = await _mcOperationsRepository.GetItemAsync("MC_ID_2");

                var selectedMeetingRoomId = employee.MeetingRoom.MeetingRoomId;
                employee.MeetingRoom = await _meetingRoomRepository.GetItemAsync(selectedMeetingRoomId);

                //TableStorage update Settings
                var entityAppSettings = await _appSettingsRepository.GetEntityAsync(OldLastNameValue, "ApplicationSettings");
                entityAppSettings.PartitionKey = employee.LastName;

                var entityProfileSettings = await _profileSettingsRepository.GetEntityAsync(OldLastNameValue, "ProfileSettings");
                entityProfileSettings.PartitionKey = employee.LastName;

                if (OldLastNameValue != employee.LastName)
                {
                    await _appSettingsRepository.UpdateEntityAsync(entityAppSettings);
                    await _appSettingsRepository.DeleteEntityAsync(OldLastNameValue, "ApplicationSettings");

                    await _profileSettingsRepository.UpdateEntityAsync(entityProfileSettings);
                    await _profileSettingsRepository.DeleteEntityAsync(OldLastNameValue, "ProfileSettings");
                }
                else
                {
                    await _appSettingsRepository.UpdateEntityAsync(entityAppSettings);
                    await _profileSettingsRepository.UpdateEntityAsync(entityProfileSettings);
                }

                await _empOperationsRepository.UpdateDocumentAsync(id, employee);

                TempData["noticeUpdated"] = "Employee was successfuly updated";
                return RedirectToAction("Index");
            }
            _logger.LogWarning("Employee in {0} IS NOT VALID", nameof(EditAsync));
            return View();
        }

        //Kontrola, jestli existuje Employee s danym LastName - Pro Validace
        public IActionResult IsLastNameUniq(string LastName, string EmployeeId)
        {
            if (EmployeeId == "undefined")
            {
                return Json(!_empOperationsRepository.GetItemsAsync().Result.Any(x => x.LastName == LastName)); // Pro Create
            }
            else
            {
                return Json(!_empOperationsRepository.GetItemsAsync().Result.Any(emp => emp.LastName == LastName && emp.EmployeeId != EmployeeId)); // Pro Edit
            }
        }

        #region Controllery pro AJAX
        //Pro AJAX, zobrazeni srovnaneho seznamu Employee podle FirstName

        public async Task<IActionResult> EmployeeFNameSorted()
        {
            return PartialView("_EmployeesSortedPartial", _empOperationsRepository.SortBy(await _empOperationsRepository.GetItemsAsync(), emp => emp.FirstName));
        }

        //Pro AJAX, zobrazeni srovnaneho seznamu Employee podle LastName
        public async Task<IActionResult> EmployeeLNameSorted()
        {
            return PartialView("_EmployeesSortedPartial", _empOperationsRepository.SortBy(await _empOperationsRepository.GetItemsAsync(), emp => emp.LastName));
        }

        //Pro AJAX, zobrazeni srovnaneho seznamu Employee podle Age
        public async Task<IActionResult> EmployeeAgeSorted()
        {
            return PartialView("_EmployeesSortedPartial", _empOperationsRepository.SortBy(await _empOperationsRepository.GetItemsAsync(), emp => emp.Age));
        }

        //Pro AJAX, zobrazeni srovnaneho seznamu Employee podle Salary
        public async Task<IActionResult> EmployeeSalarySorted()
        {
            return PartialView("_EmployeesSortedPartial", _empOperationsRepository.SortBy(await _empOperationsRepository.GetItemsAsync(), emp => emp.Salary));
        }

        //Pro AJAX, zobrazeni detailu Meeting Centra
        public async Task<IActionResult> MeetingCenterDetail(string id)
        {
            _logger.LogInformation("Getting Meeting Center with id {0} in {1}", id, nameof(MeetingCenterDetail));
            if (id == null)
            {
                _logger.LogWarning("Detail of Meeting Center with id {0} in {1} NOT FOUND", id, nameof(MeetingCenterDetail));
                return NotFound();
            }
            return PartialView("_MeetingCenterDetailPartial", await _mcOperationsRepository.GetItemAsync(id));
        }

        //Pro AJAX, zobrazeni detailu Meeting Roomu
        public async Task<IActionResult> MeetingRoomDetail(string id)
        {
            _logger.LogInformation("Getting Meeting Room with id {0} in {1}", id, nameof(MeetingRoomDetail));
            if (id == null)
            {
                _logger.LogWarning("Detail of Meeting Room with id {0} in {1} NOT FOUND", id, nameof(MeetingRoomDetail));
                return NotFound();
            }
            ViewBag.ActualRoomCapacity = _empOperationsRepository.GetActualCapacityOfRoom(await _empOperationsRepository.GetItemsAsync(), await _meetingRoomRepository.GetItemAsync(id));
            return PartialView("_MeetingRoomDetailPartial", await _meetingRoomRepository.GetItemAsync(id));
        }
        #endregion

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
