﻿using BP_Administration_System.Models.TableStorage;
using BP_Administration_System.Repositories.QueueStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Data
{
    //Trida pro inicializaci a naseedovani Queue Storage
    public class QueueStorageInitializer : QueueStorageAdapter
    {
        public QueueStorageInitializer(IOptions<StorageSettings> QueueStorageConfig, ILogger<QueueStorageAdapter> logger) : base(QueueStorageConfig, logger)
        {

        }

        //Inicializacni metoda
        public void Initialize()
        {
            CreateQueueAsync().Wait();
        }

        //Vytvori Queue pokud neexistuje
        public async Task CreateQueueAsync()
        {
            try
            {
                await queue.CreateIfNotExistsAsync();
            }
            catch (StorageException ex)
            {
                _logger.LogError("Error in {0}, Error: {1}", nameof(CreateQueueAsync), ex.ToString());
                throw ex;
            }
        }
    }
}
