﻿using BP_Administration_System.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Models
{
    public class Employee
    {
        public Employee()
        {
            DocType = "Employee";
        }

        [JsonProperty(PropertyName = "id")]
        public string EmployeeId { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        [Required, Display(Name = "First Name"), StringLength(15, MinimumLength = 1), RegularExpression(@"^[A-Z][a-z]*$",
        ErrorMessage = "The field First Name must be string without diacritics starting with capital letter")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        [Required, Display(Name = "Last Name"), StringLength(15, MinimumLength = 1), RegularExpression(@"^[A-Z][a-z]*$",
        ErrorMessage = "The field Last Name must be string without diacritics starting with capital letter")]
        [Remote("IsLastNameUniq", "Home", AdditionalFields = "EmployeeId", ErrorMessage = "Last Name already in use")]
        public string LastName { get; set; }

        [Display(Name = "Tasks")]
        public string FullName => $"{FirstName} {LastName}";

        [JsonProperty(PropertyName = "salary")]
        [Required, Display(Name = "Salary"), Range(10000, 200000)]
        public int Salary { get; set; }

        [JsonProperty(PropertyName = "age")]
        [Required, Display(Name = "Age"), Range(18, 60)]
        public int Age { get; set; }

        [JsonProperty(PropertyName = "tasks")]
        [Display(Name = "Assigned Tasks")]
        public List<string> Tasks { get; set; }

        [Display(Name = "Tasks")]
        public string TasksCommaSeperated
        {
            get { if (Tasks != null) { return String.Join(", ", Tasks); } return ""; }
        }

        [JsonProperty(PropertyName = "meetingRoom")]
        [Display(Name = "Meeting Room")]
        public MeetingRoom MeetingRoom { get; set; }

        [JsonProperty(PropertyName = "meetingCenter")]
        [Display(Name = "Available Meeting Centers")]
        public MeetingCenter MeetingCenter { get; set; }

        [JsonProperty(PropertyName = "docType")]
        public string DocType { get; set; }
    }
}
