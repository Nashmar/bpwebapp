﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BP_Administration_System.Models;
using BP_Administration_System.Repositories;
using BP_Administration_System.Repositories.BlobStorage;
using BP_Administration_System.Repositories.QueueStorage;
using BP_Administration_System.ViewModels;
using BP_Administration_System_Develop.Repositories.BlobStorage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;


namespace BP_Administration_System.Controllers
{
    public class MediaController : Controller
    {
        private readonly IPhotosRepository _photosRepository;
        private readonly IVideosRepository _videosRepository;
        private readonly IQueueStorageRepository _queueRepository;
        private readonly ILogger _logger;

        public MediaController
            (
            IPhotosRepository photosRepository,
            IVideosRepository videosRepository,
            IQueueStorageRepository queueRepository,
            ILogger<MediaController> logger
            )
        {
            _photosRepository = photosRepository;
            _videosRepository = videosRepository;
            _queueRepository = queueRepository;
            _logger = logger;
        }

        //Index Media Storage GET
        public async Task<IActionResult> MediaIndex()
        {
            return View(new BlobsViewModel
            {
                photoBlobs = await _photosRepository.GetAllBlobsAsync(),
                videoBlobs = await _videosRepository.GetAllBlobsAsync()
            });
        }

        //Detail blobu GET
        public IActionResult ImageDetail(string blobName)
        {
            _logger.LogInformation("Getting item {0} in {1}", blobName, nameof(ImageDetail));
            if (blobName == null)
            {
                _logger.LogWarning("Detail of image {0} in {1} NOT FOUND", blobName, nameof(ImageDetail));
                return NotFound();
            }
            return View(new BlobsViewModel
            {
                blob = _photosRepository.GetBlob(blobName)
            });
        }

        //Upload image GET
        [ActionName("Upload")]
        public IActionResult UploadImageAsync()
        {
            return View();
        }

        //Upload image POST
        [HttpPost]
        [ActionName("Upload")]
        public async Task<IActionResult> UploadImageAsync(IFormFile imageFile)
        {
            _logger.LogInformation("Uploading image {0} in {1}", imageFile.FileName, nameof(UploadImageAsync));

            if (imageFile == null)
            {
                _logger.LogWarning("Cannot upload image {0} in {1} - BAD REQUEST", imageFile.FileName, nameof(UploadImageAsync));
                return BadRequest();
            }
            var blobObject = await _photosRepository.UploadBlobAsync(imageFile);
            await _queueRepository.AddMessageAsync(blobObject.Name);
            TempData["noticeCreated"] = "Image was successfuly uploaded";
            return RedirectToAction("MediaIndex");
        }

        //Download image 
        [ActionName("Download")]
        public async Task<IActionResult> DownloadImageAsync(string blobName)
        {
            _logger.LogInformation("Downloading image {0} in {1}", blobName, nameof(DownloadImageAsync));

            if (blobName == null)
            {
                _logger.LogWarning("Cannot download image {0} in {1} - NOT FOUND", blobName, nameof(DownloadImageAsync));
                return NotFound();
            }
            var stream = await _photosRepository.DownloadBlobAsync(blobName);
            return File(stream.ToArray(), "application/octet-stream", blobName);
        }

        //Delete image POST
        [HttpPost]
        public async Task<IActionResult> DeleteBlobAsync(string blobName)
        {
            _logger.LogInformation("Deleting image {0} in {1}", blobName, nameof(DeleteBlobAsync));

            if (blobName == null)
            {
                _logger.LogWarning("Cannot delete image {0} in {1} - NOT FOUND", blobName, nameof(DeleteBlobAsync));
                return NotFound();
            }
            await _photosRepository.DeleteBlobAsync(blobName);

            TempData["noticeDeleted"] = "Image was successfuly deleted";
            return RedirectToAction("MediaIndex");
        }

        //Upload video GET
        [ActionName("UploadVideo")]
        public IActionResult UploadVideoAsync()
        {
            return View();
        }

        //Upload video POST
        [HttpPost]
        [ActionName("UploadVideo")]
        public async Task<IActionResult> UploadVideoAsync(IFormFile videoFile)
        {
            _logger.LogInformation("Uploading video {0} in {1}", videoFile.FileName, nameof(UploadVideoAsync));

            if (videoFile == null)
            {
                _logger.LogWarning("Cannot upload video {0} in {1} - BAD REQUEST", videoFile.FileName, nameof(UploadVideoAsync));
                return BadRequest();
            }
            var blobObject = await _videosRepository.UploadBlobAsync(videoFile);
            TempData["noticeCreated"] = "Video was successfuly uploaded";
            return RedirectToAction("MediaIndex");
        }

        //Download video 
        [ActionName("DownloadVideo")]
        public async Task<IActionResult> DownloadVideoAsync(string blobName)
        {
            _logger.LogInformation("Downloading video {0} in {1}", blobName, nameof(DownloadVideoAsync));

            if (blobName == null)
            {
                _logger.LogWarning("Cannot download video {0} in {1} - NOT FOUND", blobName, nameof(DownloadVideoAsync));
                return NotFound();
            }
            var stream = await _videosRepository.DownloadBlobAsync(blobName);
            return File(stream.ToArray(), "application/octet-stream", blobName);
        }

        //Delete video POST
        [HttpPost]
        public async Task<IActionResult> DeleteVideoBlobAsync(string blobName)
        {
            _logger.LogInformation("Deleting video {0} in {1}", blobName, nameof(DeleteVideoBlobAsync));

            if (blobName == null)
            {
                _logger.LogWarning("Cannot delete video {0} in {1} - NOT FOUND", blobName, nameof(DeleteVideoBlobAsync));
                return NotFound();
            }
            await _videosRepository.DeleteBlobAsync(blobName);

            TempData["noticeDeleted"] = "Video was successfuly deleted";
            return RedirectToAction("MediaIndex");
        }
    }
}