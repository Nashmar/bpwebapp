﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace BP_Administration_System.Models.TableStorage
{
    public class ApplicationSettings : TableEntity
    {
        public ApplicationSettings()
        {

        }

        public ApplicationSettings(string employeeLastName, string appSettingsId)
        {
            this.PartitionKey = employeeLastName;
            this.RowKey = appSettingsId;
        }
        [Display(Name = "Design Theme"), RegularExpression(@"\b(Light|Dark|\s)\b$",
            ErrorMessage = "The field Design Theme must be empty or one of strings Light or Dark")]
        public string DesignTheme { get; set; }

        [RegularExpression(@"\b(EN|CZ|DE|\s)\b$",
            ErrorMessage = "The field Design Theme must be empty or one of strings EN,CZ,DE")]
        public string Language { get; set; }
    }
}
