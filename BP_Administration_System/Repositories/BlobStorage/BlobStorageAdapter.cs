﻿using BP_Administration_System.Models.TableStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System_Develop.Repositories.BlobStorage
{
    //Abstraktni trida pro komunikaci s Queue Storage
    public abstract class BlobStorageAdapter
    {
        protected readonly string azureStorageConnectionString;
        protected CloudBlobContainer container;
        protected CloudBlobClient blobClient;
        protected CloudStorageAccount storageAccount;
        protected readonly ILogger _logger;

        public BlobStorageAdapter(IOptions<StorageSettings> BlobStorageConfig, string specificContainer, ILogger<BlobStorageAdapter> logger)
        {
            this.azureStorageConnectionString = BlobStorageConfig.Value.AzureStorageConnectionString;
            storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);
            blobClient = storageAccount.CreateCloudBlobClient();
            this.container = blobClient.GetContainerReference(specificContainer);
            _logger = logger;
        }
    }
}
