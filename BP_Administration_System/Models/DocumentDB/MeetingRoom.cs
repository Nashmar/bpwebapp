﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Models
{
    [JsonObject(IsReference = true)]
    public class MeetingRoom
    {
        [JsonProperty(PropertyName = "id")]
        public string MeetingRoomId { get; set; }

        [JsonProperty(PropertyName = "name")]
        [Display(Name = "Meeting Room Name"), StringLength(30, MinimumLength = 1)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        [Display(Name = "Description"), StringLength(30, MinimumLength = 1)]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "capacity")]
        [Display(Name = "Actual Capacity")]
        public int Capacity { get; set; }

        [JsonProperty(PropertyName = "meetingCenter")]
        [Display(Name = "Meeting Center Name")]
        public MeetingCenter MeetingCenter { get; set; }

        [JsonProperty(PropertyName = "docType")]
        public string DocType { get; set; }
    }
}
