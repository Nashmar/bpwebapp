﻿using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BP_Administration_System.Repositories.QueueStorage
{
    //Interface zakladnich operaci
    public interface IQueueStorageRepository
    {
        Task AddMessageAsync(string message);
    }
}
